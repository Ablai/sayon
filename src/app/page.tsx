"use client";
import Image from "next/image";
import Link from "next/link";
import styles from "./page.module.css";
import { clsx } from "clsx";
import {useEffect, useState} from "react";
import Box from "@mui/material/Box";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import HomeIcon from "@mui/icons-material/Home";
import ScheduleIcon from "@mui/icons-material/Schedule";
import PersonIcon from "@mui/icons-material/Person";

import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";

import { useRouter } from 'next/navigation'

export default function Home() {
  const [value, setValue] = useState(0);
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);

    const router = useRouter()

  const iOS =
    typeof navigator !== "undefined" &&
    /iPad|iPhone|iPod/.test(navigator.userAgent);
  const toggleDrawer = (open: boolean) => () => {
    setIsDrawerOpen(open);
  };

  const login = async () => {
      const response = await fetch('http://localhost:8090/api/v1/auth/login-anon', {
          method: 'POST'
      });
      const { data } = await response.json();
      console.log(data)
      localStorage.setItem('token', data.token);
      localStorage.setItem('wsToken', data.ws_token);
      localStorage.setItem('userId', data.id);
  }

    useEffect(() => {
        login();
    }, []);

  const createChat = async () => {
      const response = await fetch('http://localhost:8090/api/v1/chats', {
          method: 'POST',
          headers: {
              "Authorization": `Bearer ${localStorage.getItem('token')}`,
              "Content-Type": "application/json",
          },
          body: JSON.stringify({ assistant_id: 1 })
      });
      const { data } = await response.json();

      router.push(`/chat/${data.id}`)
  }

  const list = (
    <Box
      sx={{ width: "auto" }}
      role="presentation"
      onClick={toggleDrawer(false)}
    >
      <List>
        {["Английский язык", "Русский язык", "Казахский язык"].map(
          (text, index) => (
              <ListItem key={text} disablePadding onClick={() => createChat()}>
                <ListItemButton>
                  <ListItemText primary={text} />
                </ListItemButton>
              </ListItem>
          )
        )}
      </List>
    </Box>
  );

  return (
    <div className={styles.wrapper}>
      <header className={clsx("flex justify-between", styles.header)}>
        <div className="logo flex">
          <Image
            src="/logo.svg"
            alt="Logo"
            className="mr-2"
            width={95}
            height={21}
            priority
          />
        </div>
      </header>

      <h1 className="text-lg font-semibold my-5">С кем хотите поговорить?</h1>

      <div className={styles.card} onClick={toggleDrawer(true)}>
        <Image src="/tutor.svg" alt="tutor" width={40} height={40} priority />
        <div>
          <h4 className="text-lg font-semibold">Репетитор</h4>
          <p className={clsx("text-sm", styles.cardDesc)}>
            Найдите себе учителя по любой специальности
          </p>
        </div>
      </div>

      <div className={styles.card} onClick={toggleDrawer(true)}>
        <Image
          src="/psychologist.svg"
          alt="psychologist"
          width={40}
          height={40}
          priority
        />
        <div>
          <h4 className="text-lg font-semibold">Психолог</h4>
          <p className={clsx("text-sm", styles.cardDesc)}>
            Поговорите с собеседником, если все сложно
          </p>
        </div>
      </div>

      <div className={styles.card} onClick={toggleDrawer(true)}>
        <Image
          src="/native-speaker.svg"
          alt="native speaker"
          width={40}
          height={40}
          priority
        />
        <div>
          <h4 className="text-lg font-semibold">Носитель языка</h4>
          <p className={clsx("text-sm", styles.cardDesc)}>
            Поговорите с собеседником владеющим инностранным языком
          </p>
        </div>
      </div>

      <SwipeableDrawer
        anchor="bottom"
        open={isDrawerOpen}
        disableBackdropTransition={!iOS}
        disableDiscovery={iOS}
        onClose={toggleDrawer(false)}
        onOpen={toggleDrawer(true)}
      >
        {list}
      </SwipeableDrawer>

      <Box className={styles.bottomNav}>
        <BottomNavigation
          showLabels
          value={value}
          onChange={(event, newValue) => {
            setValue(newValue);
          }}
        >
          <BottomNavigationAction label="Главная" icon={<HomeIcon />} />
          <BottomNavigationAction label="История" icon={<ScheduleIcon />} />
          <BottomNavigationAction label="Аккаунт" icon={<PersonIcon />} />
        </BottomNavigation>
      </Box>
    </div>
  );
}
