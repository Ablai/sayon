"use client";
import Image from "next/image";
import Link from "next/link";
import styles from "./page.module.css";
import { useState, useEffect } from "react";
import { Centrifuge } from 'centrifuge';

import { audioRecorder } from "./audioRecorder";

export default function Chat({ params }: { params: { id: string } }) {
  const [isRecording, setIsRecording] = useState(false);
  const [chat, setChat] = useState();

  function subscribe() {
    console.log('sub func')
    const centrifuge = new Centrifuge("ws://localhost:8031/connection/websocket", {
          token: localStorage.getItem('wsToken')
        }
    );
    const sub = centrifuge.newSubscription(`chat_${params.id}#${localStorage.getItem('userId')}`);
    sub.on('publication', function(ctx) {
      console.log(ctx)
      console.log(ctx.data);
      console.log(ctx.data.value)
    });

    sub.subscribe();

    centrifuge.on('connecting', function (ctx) {
      console.log(`connecting: ${ctx.code}, ${ctx.reason}`);
    }).on('connected', function (ctx) {
      console.log(`connected over ${ctx.transport}`);
    }).on('disconnected', function (ctx) {
      console.log(`disconnected: ${ctx.code}, ${ctx.reason}`);
    }).connect();

  }

  const getChat = async () => {
    const response = await fetch('http://localhost:8090/api/v1/chats', {
      headers: {
        "Authorization": `Bearer ${localStorage.getItem('token')}`,
      },
    });
    const { data } = await response.json();
    setChat(data);
    subscribe();
  }

  useEffect(() => {
    audioRecorder.firstPaint();
    getChat();
  }, []);

  const handleRecording = async () => {
    if (!isRecording) {
      audioRecorder.start();
    } else {
      const blob = await audioRecorder.stop();
      const formData = new FormData();
      formData.append('speech', blob, 'sosat');
      const response = fetch(`http://localhost:8090/api/v1/chats/${params.id}/messages/`, {
        method: 'POST',
        headers: {
          "Authorization": `Bearer ${localStorage.getItem('token')}`,
          "Content-Type": "multipart/form-data",
        },
        body: formData
      })
    }
    setIsRecording(!isRecording);
  };

  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <Link href="/">
          <Image
            src="/arrow-left.svg"
            alt="arrow left"
            width={24}
            height={24}
            priority
          />
        </Link>
        <div className="text-white">
          <div>Репетитор</div>
          <div className="text-xs">Англйиский язык</div>
        </div>
      </div>
      <div className={styles.canvasWrapper}>
        <canvas
          id="visualization"
          className={styles.canvas}
          width="120px"
          height="120px"
        ></canvas>
      </div>
      <div className={styles.recorderBtn} onClick={handleRecording}>
        <Image
          src={isRecording ? "/pause.svg" : "/play.svg"}
          alt="handle recording"
          width={60}
          height={60}
          priority
        />
      </div>
    </div>
  );
}
