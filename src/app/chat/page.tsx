"use client";
import Image from "next/image";
import Link from "next/link";
import styles from "./page.module.css";
import { useState, useEffect } from "react";

import { audioRecorder } from "./audioRecorder";

export default function Chat() {
  const [isRecording, setIsRecording] = useState(false);

  useEffect(() => {
    audioRecorder.firstPaint();
  }, []);

  const handleRecording = async () => {
    if (!isRecording) {
      audioRecorder.start();
    } else {
      const blob = await audioRecorder.stop();
      const response = fetch('http://localhost:8090/api/')
    }
    setIsRecording(!isRecording);
  };

  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <Link href="/">
          <Image
            src="/arrow-left.svg"
            alt="arrow left"
            width={24}
            height={24}
            priority
          />
        </Link>
        <div className="text-white">
          <div>Репетитор</div>
          <div className="text-xs">Англйиский язык</div>
        </div>
      </div>
      <div className={styles.canvasWrapper}>
        <canvas
          id="visualization"
          className={styles.canvas}
          width="120px"
          height="120px"
        ></canvas>
      </div>
      <div className={styles.recorderBtn} onClick={handleRecording}>
        <Image
          src={isRecording ? "/pause.svg" : "/play.svg"}
          alt="handle recording"
          width={60}
          height={60}
          priority
        />
      </div>
    </div>
  );
}
